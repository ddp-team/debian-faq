<?xml version='1.0' encoding='utf-8'?>
<!-- -*- DocBook -*- -->
<chapter id="faqinfo"><title>Ogólne informacje dotyczące Często Zadawanych Pytań (FAQ)</title>
<section id="authors"><title>Autorzy</title>
<para>
Pierwszym wydaniem tego FAQ zajmowali się J.H.M.  Dassen (Ray) i Chuck
Stickelman.  Autorami odnowionego FAQ systemu Debian GNU/Linux są: Susan G.
Kleinmann i Sven Rudolph.  Po nich FAQ zajmował się Santiago Vila.
Koordynatorem obecnej wersji jest Josip Rodin.
</para>
<para>
Część informacji zaczerpnięto z:
</para>
<itemizedlist>
<listitem>
<para>
The Debian-1.1 release announcement, autorstwa <ulink
url="http://www.perens.com/">Brucea Perensa</ulink>.
</para>
</listitem>
<listitem>
<para>
The Linux FAQ, autorstwa <ulink
url="http://www.chiark.greenend.org.uk/~ijackson/">Iana Jacksona</ulink>.
</para>
</listitem>
<listitem>
<para>
<ulink url="http://lists.debian.org/">Debian Mailing Lists Archives</ulink>,
</para>
</listitem>
<listitem>
<para>
[FIXME] the dpkg programmers' manual and the Debian Policy manual (see <xref
linkend="debiandocs"/>)
</para>
</listitem>
<listitem>
<para>
od wielu deweloperów, ochotników, beta-testerów oraz
</para>
</listitem>
<listitem>
<para>
niespodziewanych wspomnień autorów.  :-)
</para>
</listitem>
</itemizedlist>
<para>
Autorzy pragną podziękować wszystkim tym, którzy pomogli stworzyć ten
dokument.
</para>
<para>
Reklamacji nie uwzględnia się.  Znaki handlowe są zastrzeżone.
</para>
</section>

<section id="translators"><title>Tłumacze</title>
<para>
Do powstania polskiej wersji tego dokumentu przyczyniły się następujące
osoby z projektu <ulink url="http://debian.linux.org.pl">PDDP</ulink>:
</para>
<itemizedlist>
<listitem>
<para>
Marcin Andruszkiewicz
</para>
</listitem>
<listitem>
<para>
Marcin Betlej - <email>marbej@debian.linux.org.pl</email>
</para>
</listitem>
<listitem>
<para>
Mariusz Centka - <email>mariusz.centka@debian.linux.org.pl</email>
</para>
</listitem>
<listitem>
<para>
Karol Czachorowski
</para>
</listitem>
<listitem>
<para>
Tomasz Dziedzic
</para>
</listitem>
<listitem>
<para>
Paweł Ekert
</para>
</listitem>
<listitem>
<para>
Bartosz Feński - <email>fenio@debian.linux.org.pl</email> - koordynator
tłumaczenia
</para>
</listitem>
<listitem>
<para>
Radosław Grzanka - <email>radekg@debian.linux.org.pl</email>
</para>
</listitem>
<listitem>
<para>
Bartosz 'Xebord' Janowski
</para>
</listitem>
<listitem>
<para>
Dominik Juszczyk - <email>djus@debian.linux.org.pl</email>
</para>
</listitem>
<listitem>
<para>
Marcin Paweł Kobierzycki - <email>m-kobierzycki@debian.linux.org.pl</email>
</para>
</listitem>
<listitem>
<para>
Maciej Krzymiński
</para>
</listitem>
<listitem>
<para>
Marcin Kuras - <email>kura@debian.linux.org.pl</email>
</para>
</listitem>
<listitem>
<para>
Jacek Lachowicz
</para>
</listitem>
<listitem>
<para>
Bartłomiej "MYCHA" Mroczkowski - <email>mycha@debian.linux.org.pl</email>
</para>
</listitem>
<listitem>
<para>
Tomasz Z.  Napierała - <email>zen@debian.linux.org.pl</email>
</para>
</listitem>
<listitem>
<para>
Marcin Płaneta - <email>welnian@debian.linux.org.pl</email>
</para>
</listitem>
<listitem>
<para>
Mateusz Prichacz - <email>mateusz@debian.linux.org.pl</email>
</para>
</listitem>
<listitem>
<para>
Marcin Rogowski
</para>
</listitem>
<listitem>
<para>
Sławomir Siejka
</para>
</listitem>
<listitem>
<para>
Michał Skuza
</para>
</listitem>
<listitem>
<para>
Dominik Stodolny - <email>phobos@debian.linux.org.pl</email>
</para>
</listitem>
<listitem>
<para>
Przemysław Adam Śmiejek <email>tristan@debian.linux.org.pl</email>
</para>
</listitem>
<listitem>
<para>
Grzegorz Witkowski
</para>
</listitem>
<listitem>
<para>
Krzysztof Witkowski <email>tjup@debian.linux.org.pl</email>
</para>
</listitem>
<listitem>
<para>
Bartosz Zapałowski <email>zapal@debian.linux.org.pl</email>
</para>
</listitem>
</itemizedlist>
</section>

<section id="feedback"><title>Informacje zwrotne</title>
<para>
Komentarze i uzupełnienia tego dokumentu są zawsze mile widziane.  Prosimy o
wysłanie listu na adres <email>doc-debian@packages.debian.org</email>, albo
dopisanie do listy życzeń dotyczących pakietu <systemitem
role="package">doc-debian</systemitem>.
</para>
</section>

<section id="latest"><title>Dostępność</title>
<para>
Najnowsza wersja niniejszego FAQ jest do poczytania na stronach WWW Debiana pod
adresem <ulink
url="http://www.debian.org/doc/FAQ/">http://www.debian.org/doc/FAQ/</ulink>.
</para>
<para>
FAQ można również pobrać w postaci czystego pliku tekstowego, pliku HTML, w
formacie PostScript albo PDF z <ulink
url="http://www.debian.org/doc/user-manuals#faq">http://www.debian.org/doc/user-manuals#faq</ulink>.
Znajduje się tam w kilku wersjach językowych.
</para>
<para>
Oryginalne pliki SGML użyte do stworzenia tej dokumentacji są dostępne w
pakiecie źródłowym <systemitem role="package">doc-debian</systemitem> albo w
repozytorium CVS:
<literal>:pserver:anonymous@cvs.debian.org:/cvs/debian-doc/ddp/manuals.sgml/faq</literal>
</para>
</section>

<section id="docformat"><title>Format dokumentu</title>
<para>
Ten dokument został napisany przy pomocy DebianDoc SGML DTD (utworzonego na
podstawie LinuxDoc SGML).  System DebianDoc umożliwia nam tworzenie plików w
wielu formatach na podstawie jednego źródła, tzn.  ten dokument może być
oglądany jako strona internetowa, czysty tekst, TeX DVI, PostScript, PDF albo
GNU Info.
</para>
<para>
Narzędzia konwertujące dla formatu DebianDoc SGML są dostępne w pakiecie
<systemitem role="package">debiandoc-sgml</systemitem>.
</para>
</section>

</chapter>

