# Translation of debian-faq manpage to European Portuguese
#
# Copyright (C) 2019 Free Software Foundation, Inc.
# This file is distributed under the same license as the debian-faq package.
#
# Américo Monteiro <a_monteiro@gmx.com>, 2019, 2021.
msgid ""
msgstr ""
"Project-Id-Version: debian-faq 11 uptodate\n"
"POT-Creation-Date: 2024-11-11 21:02+0100\n"
"PO-Revision-Date: 2021-11-22 20:20+0000\n"
"Last-Translator: Américo Monteiro <a_monteiro@gmx.com>\n"
"Language-Team: Portuguese <>\n"
"Language: pt\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-Generator: Lokalize 20.12.0\n"

#. type: Content of: <chapter><title>
#: en/uptodate.dbk:8
msgid "Keeping your Debian system up-to-date"
msgstr "Manter o seu sistema Debian actualizado"

#. type: Content of: <chapter><para>
#: en/uptodate.dbk:10
msgid ""
"One of Debian's goals is to provide a consistent upgrade path and a secure "
"upgrade process.  We always do our best to make upgrading to new releases a "
"smooth procedure.  In case there's some important note to add to the upgrade "
"process, the packages will alert the user, and often provide a solution to a "
"possible problem."
msgstr ""
"Um dos objectivos de Debian é fornecer um caminho de actualização "
"consistente e um processo de actualização seguro. Nós fazemos sempre o nosso "
"melhor para tornar a actualização para novos lançamentos um processo suave. "
"Nos casos em que existe alguma nota importante a adicionar ao processo de "
"actualização, os pacotes irão alertar o utilizador, e muitas vezes fornecer "
"uma solução para um possível problema."

#. type: Content of: <chapter><para>
#: en/uptodate.dbk:17
msgid ""
"You should also read the Release Notes document that describes the details "
"of specific upgrades.  It is available on the Debian website at <ulink "
"url=\"&url-debian-releasenotes;\"/> and is also shipped on the Debian CDs, "
"DVDs and Blu-ray discs."
msgstr ""
"Você deve também ler o documento Release Notes que descreve os detalhes de "
"actualizações específicas. Está disponível no sitio web de Debian em <ulink "
"url=\"&url-debian-releasenotes;\"/> e é também lançado com os CDs, DVDs e "
"discos Blu-ray de Debian."

#. type: Content of: <chapter><section><title>
#: en/uptodate.dbk:21
msgid "How can I keep my Debian system current?"
msgstr "Como é que Eu consigo manter o meu sistema Debian actual?"

#. type: Content of: <chapter><section><para>
#: en/uptodate.dbk:23
msgid ""
"One could simply visit a Debian archive site, then peruse the directories "
"until one finds the desired file, and then fetch it, and finally install it "
"using <literal>dpkg</literal>.  Note that <literal>dpkg</literal> will "
"install upgrade files in place, even on a running system.  Sometimes, a "
"revised package will require the installation of a newly revised version of "
"another package, in which case the installation will fail until/unless the "
"other package is installed."
msgstr ""
"Pode simplesmente visitar um sítio de arquivo Debian, depois percorrer os "
"directórios até encontrar o ficheiro desejado, depois obter-lo, e finalmente "
"instala-lo usando o <literal>dpkg</literal>.  Note que o <literal>dpkg</"
"literal> irá instalar ficheiros de actualização no lugar, mesmo num sistema "
"a correr. Por vezes, um pacote revisão poderá requerer a instalação duma "
"nova versão revisada de outro pacote, e neste caso a irá falhar a menos ou "
"até que que o outro pacote seja instalado."

#. type: Content of: <chapter><section><para>
#: en/uptodate.dbk:32
msgid ""
"Many people find this approach much too time-consuming, since Debian evolves "
"so quickly -- typically, a dozen or more new packages are uploaded every "
"week.  This number is larger just before a new major release.  To deal with "
"this avalanche, many people prefer to use a more automated method.  Several "
"different packages are available for this purpose:"
msgstr ""
"Muitas pessoas acham este método muito consumidor de tempo, pois Debian "
"evolui muito rapidamente -- tipicamente, uma dúzia ou mais de pacotes são "
"enviados todas as semanas. Este número é maior logo antes dos grandes "
"lançamentos. Para lidar come esta avalanche, muita gente prefere usar um "
"método mais automatizado. Estão disponíveis vários pacotes diferentes para "
"este propósito."

#. type: Content of: <chapter><section><section><title>
#: en/uptodate.dbk:39
msgid "aptitude"
msgstr "aptitude"

#. type: Content of: <chapter><section><section><para>
#: en/uptodate.dbk:41
msgid ""
"<command>aptitude</command> is the recommended package manager for &debian; "
"systems, and is described in <xref linkend=\"aptitude\"/>."
msgstr ""
"<command>aptitude</command> é o gestor de pacotes recomendado para sistemas "
"&debian;, e está descrito em <xref linkend=\"aptitude\"/>."

#. type: Content of: <chapter><section><section><para>
#: en/uptodate.dbk:45
msgid ""
"Before you can use <command>aptitude</command> to make an upgrade, you'll "
"have to edit the <literal>/etc/apt/sources.list</literal> file to set it "
"up.  If you wish to upgrade to the latest stable version of Debian, you'll "
"probably want to use a source like this one:"
msgstr ""
"Antes de você poder usar o <command>aptitude</command> para fazer uma "
"actualização, você tem de editar o ficheiro <literal>/etc/apt/sources.list</"
"literal> para o configurar. Se você deseja actualizar para a versão stable "
"de Debian, você irá provavelmente querer usar uma fonte como esta:"

#. type: Content of: <chapter><section><section><screen>
#: en/uptodate.dbk:51
#, fuzzy, no-wrap
#| msgid "http://ftp.us.debian.org/debian stable main contrib\n"
msgid "http://deb.debian.org/debian stable main contrib\n"
msgstr "http://ftp.us.debian.org/debian stable main contrib\n"

#. type: Content of: <chapter><section><section><para>
#: en/uptodate.dbk:54
#, fuzzy
#| msgid ""
#| "You can replace ftp.us.debian.org (the mirror in the United States) with "
#| "the name of a faster Debian mirror near you.  See the mirror list at "
#| "<ulink url=\"https://www.debian.org/mirror/list\"/> for more information."
msgid ""
"The mirror <ulink url=\"https://deb.debian.org/\"/> is backed by a content-"
"delivery network and requests to it will be directed to the closest instance "
"to you. If you have a faster Debian mirror close to you, you can replace "
"deb.debian.org with that one. See the mirror list at <ulink url=\"https://"
"www.debian.org/mirror/list\"/> for more information."
msgstr ""
"Você pode substituir ftp.us.debian.org (o espelho nos Estados Unidos) pelo "
"nome de um espelho Debian mais rápido perto de si. Veja a lista de espelhos "
"em <ulink url=\"https://www.debian.org/mirror/list\"/> para mais informação."

#. type: Content of: <chapter><section><section><para>
#: en/uptodate.dbk:61
msgid ""
"Or you can use the redirector service httpredir.debian.org which aims to "
"solve the problem of choosing a Debian mirror.  It uses the geographic "
"location of the user and other information to choose the best mirror that "
"can serve the files.  To take advantage of it use a source like this one:"
msgstr ""
"Ou você pode usar o serviço de re-direção httpredir.debian.org cujo "
"objectivo é resolver o problemas de escolher um espelho Debian. Usa a "
"localização geográfica do utilizador e outras informações para escolher o "
"melhor espelho que possa servir os ficheiros. Para usufruir dele use uma "
"linha de fonte como esta:"

#. type: Content of: <chapter><section><section><screen>
#: en/uptodate.dbk:67
#, no-wrap
msgid "http://httpredir.debian.org/debian stable main contrib\n"
msgstr "http://httpredir.debian.org/debian stable main contrib\n"

#. type: Content of: <chapter><section><section><para>
#: en/uptodate.dbk:70
msgid ""
"More details on this can be found in the "
"<citerefentry><refentrytitle>sources.list</refentrytitle><manvolnum>5</"
"manvolnum></citerefentry> manual page."
msgstr ""
"Mais detalhes sobre isto pode ser encontrados no manual "
"<citerefentry><refentrytitle>sources.list</refentrytitle><manvolnum>5</"
"manvolnum></citerefentry>"

#. type: Content of: <chapter><section><section><para>
#: en/uptodate.dbk:75
msgid "To update your system from the command line, run"
msgstr "Para actualizar o seu sistema a partir da linha de comandos, corra"

#. type: Content of: <chapter><section><section><screen>
#: en/uptodate.dbk:78
#, no-wrap
msgid "aptitude update\n"
msgstr "aptitude update\n"

#. type: Content of: <chapter><section><section><para>
#: en/uptodate.dbk:81 en/uptodate.dbk:127
msgid "followed by"
msgstr "seguido por"

#. type: Content of: <chapter><section><section><screen>
#: en/uptodate.dbk:84
#, no-wrap
msgid "aptitude full-upgrade\n"
msgstr "aptitude full-upgrade\n"

#. type: Content of: <chapter><section><section><para>
#: en/uptodate.dbk:87
msgid ""
"Answer any questions that might come up, and your system will be upgraded."
msgstr ""
"Responda a quaisquer questões que possam aparecer, e o seu sistema será "
"actualizado."

#. type: Content of: <chapter><section><section><para>
#: en/uptodate.dbk:90
msgid ""
"Note that <command>aptitude</command> is not the recommended tool for doing "
"upgrades from one &debian; release to another.  Use <command>apt-get</"
"command> instead.  For upgrades between releases you should read the <ulink "
"url=\"&url-debian-releasenotes;\">Release Notes</ulink>.  This document "
"describes in detail the recommended steps for upgrades from previous "
"releases as well as known issues you should consider before upgrading."
msgstr ""
"Note que o <command>aptitude</command> não é a ferramenta recomendada para "
"fazer actualizações de um lançamento &debian; para outro. Em vez deste use o "
"<command>apt-get</command>. Para actualizações entre lançamentos você deve "
"ler as <ulink url=\"&url-debian-releasenotes;\">Release Notes</ulink>.  Este "
"documento descreve em detalhe os passos recomendados para actualizações a "
"partir de lançamentos anteriores assim como problemas conhecidos que deve "
"considerar antes de actualizar."

#. type: Content of: <chapter><section><section><para>
#: en/uptodate.dbk:99
msgid ""
"For details, see the manual page <citerefentry><refentrytitle>aptitude</"
"refentrytitle><manvolnum>8</manvolnum></citerefentry>, and the file "
"<filename>/usr/share/aptitude/README</filename>."
msgstr ""
"Para detalhes, veja o manual do <citerefentry><refentrytitle>aptitude</"
"refentrytitle><manvolnum>8</manvolnum></citerefentry>, e o ficheiro "
"<filename>/usr/share/aptitude/README</filename>."

#. type: Content of: <chapter><section><section><title>
#: en/uptodate.dbk:105
msgid "apt-get and apt-cdrom"
msgstr "apt-get e apt-cdrom"

#. type: Content of: <chapter><section><section><para>
#: en/uptodate.dbk:107
msgid ""
"An alternative to <command>aptitude</command> is <command>apt-get</command> "
"which is an APT-based command-line tool (described previously in <xref "
"linkend=\"apt-get\"/>)."
msgstr ""
"Uma alternativa ao <command>aptitude</command> é o <command>apt-get</"
"command> o qual é um,a ferramenta de linha de comandos baseada no APT "
"(descrita previamente em <xref linkend=\"apt-get\"/>)."

#. type: Content of: <chapter><section><section><para>
#: en/uptodate.dbk:112
msgid ""
"<command>apt-get</command>, the APT-based command-line tool for handling "
"packages, provides a simple, safe way to install and upgrade packages."
msgstr ""
"<command>apt-get</command>, a ferramenta de linha de comandos baseada no APT "
"para lidar com pacotes, fornece uma maneira simples e segura de instalar e "
"actualizar pacotes."

#. type: Content of: <chapter><section><section><para>
#: en/uptodate.dbk:116
msgid ""
"To use <command>apt-get</command>, edit the <literal>/etc/apt/sources.list</"
"literal> file to set it up, just as for <xref linkend=\"aptitude-upgrade\"/>."
msgstr ""
"Para usar o <command>apt-get</command>, edit o ficheiro <literal>/etc/apt/"
"sources.list</literal> para o configurar, tal como para o <xref "
"linkend=\"aptitude-upgrade\"/>."

#. type: Content of: <chapter><section><section><para>
#: en/uptodate.dbk:121
msgid "Then run"
msgstr "Depois corra"

#. type: Content of: <chapter><section><section><screen>
#: en/uptodate.dbk:124
#, no-wrap
msgid "apt-get update\n"
msgstr "apt-get update\n"

#. type: Content of: <chapter><section><section><screen>
#: en/uptodate.dbk:130
#, no-wrap
msgid "apt-get dist-upgrade\n"
msgstr "apt-get dist-upgrade\n"

#. type: Content of: <chapter><section><section><para>
#: en/uptodate.dbk:133
msgid ""
"Answer any questions that might come up, and your system will be upgraded.  "
"See also the <citerefentry><refentrytitle>apt-get</"
"refentrytitle><manvolnum>8</manvolnum></citerefentry> manual page, as well "
"as <xref linkend=\"apt-get\"/>."
msgstr ""
"Responda a quaisquer perguntas que possam surgir, e o seu sistema ficará "
"actualizado. Veja também o manual <citerefentry><refentrytitle>apt-get</"
"refentrytitle><manvolnum>8</manvolnum></citerefentry>, assim como o <xref "
"linkend=\"apt-get\"/>."

#. type: Content of: <chapter><section><section><para>
#: en/uptodate.dbk:139
msgid ""
"If you want to use CDs/DVDs/BDs to install packages, you can use "
"<command>apt-cdrom</command>.  For details, please see the Release Notes, "
"section \"Adding APT sources from optical media\"."
msgstr ""
"Se você desejar usar CDs/DVDs/BDs para instalar pacotes, você pode usar o "
"<command>apt-cdrom</command>.  Para detalhes, por favor veja as Notas de "
"Lançamento, secção \"Adicionar fontes APT a partir de meios óticos\"."

#. type: Content of: <chapter><section><section><para>
#: en/uptodate.dbk:144
msgid ""
"Please note that when you get and install the packages, you'll still have "
"them kept in your /var directory hierarchy.  To keep your partition from "
"overflowing, remember to delete extra files using <literal>apt-get clean</"
"literal> and <literal>apt-get autoclean</literal>, or to move them someplace "
"else (hint: use <systemitem role=\"package\">apt-move</systemitem>)."
msgstr ""
"Por favor note que quando você obtém e instala os pacotes, você vai "
"continuar a manter-los na sua hierarquia do directório /var. Para impedir "
"que a sua partição fique sem espaço, lembre-se de apagar os ficheiros extra "
"usando <literal>apt-get clean</literal> e <literal>apt-get autoclean</"
"literal>, ou então move-los para outro local (dica: use <systemitem "
"role=\"package\">apt-move</systemitem>)."

#. type: Content of: <chapter><section><title>
#: en/uptodate.dbk:154
msgid "Must I go into single user mode in order to upgrade a package?"
msgstr ""
"Tenho que ir para modo de único-utilizador para a actualizar um pacote?"

#. type: Content of: <chapter><section><para>
#: en/uptodate.dbk:156
msgid ""
"No.  Packages can be upgraded in place, even in running systems.  Debian has "
"a <literal>start-stop-daemon</literal> program that is invoked to stop, then "
"restart running process if necessary during a package upgrade."
msgstr ""
"Não. Os pacotes podem ser actualizados no local, mesmo em sistemas a correr. "
"Debian tem um programa <literal>start-stop-daemon</literal> que é invocado "
"para parar, depois reiniciar os processos a correr se necessário durante a "
"actualização dum pacote."

#. type: Content of: <chapter><section><title>
#: en/uptodate.dbk:162
msgid "Do I have to keep all those .deb archive files on my disk?"
msgstr "Tenho que manter todos aqueles ficheiros arquivo .deb no meu disco?"

#. type: Content of: <chapter><section><para>
#: en/uptodate.dbk:164
msgid ""
"No.  If you have downloaded the files to your disk then after you have "
"installed the packages, you can remove them from your system, e.g. by "
"running <literal>aptitude clean</literal>."
msgstr ""
"Não. Se você descarregou os ficheiros para o seu disco então após ter "
"instalado os pacotes, pode remove-los do seu sistema, por exemplo, ao correr "
"<literal>aptitude clean</literal>."

#. type: Content of: <chapter><section><title>
#: en/uptodate.dbk:170
msgid ""
"How can I keep a log of the packages I added to the system? I'd like to know "
"when upgrades and removals have occurred and on which packages!"
msgstr ""
"Como é que Eu mantenho um registo dos pacotes que Eu adiciono ao sistema? EU "
"gostava de saber quando as actualizações e remoções ocorreram e em quais "
"pacotes!"

#. type: Content of: <chapter><section><para>
#: en/uptodate.dbk:172
msgid ""
"Passing the <literal>--log</literal>-option to <command>dpkg</command> makes "
"<command>dpkg</command> log status change updates and actions.  It logs both "
"the <command>dpkg</command>-invokation (e.g."
msgstr ""
"Passar a opção <literal>--log</literal>- ao <command>dpkg</command> faz com "
"que o <command>dpkg</command> registe alterações de estados, actualizações e "
"acções. Ele regista ambos, a invocação do <command>dpkg</command> (ex."

#. type: Content of: <chapter><section><screen>
#: en/uptodate.dbk:177
#, no-wrap
msgid "2005-12-30 18:10:33 install hello 1.3.18 2.1.1-4\n"
msgstr "2005-12-30 18:10:33 install hello 1.3.18 2.1.1-4\n"

#. type: Content of: <chapter><section><para>
#: en/uptodate.dbk:180
msgid ") and the results (e.g."
msgstr ") e os resultados (ex."

#. type: Content of: <chapter><section><screen>
#: en/uptodate.dbk:183
#, no-wrap
msgid "2005-12-30 18:10:35 status installed hello 2.1.1-4\n"
msgstr "2005-12-30 18:10:35 status installed hello 2.1.1-4\n"

#. type: Content of: <chapter><section><para>
#: en/uptodate.dbk:186
msgid ""
") If you'd like to log all your <command>dpkg</command> invocations (even "
"those done using frontends like <command>aptitude</command>), you could add"
msgstr ""
") Se você desejar registar todas as suas invocações do <command>dpkg</"
"command> (mesmo aquelas feitas usando frontends como o <command>aptitude</"
"command>), você pode adicionar"

#. type: Content of: <chapter><section><screen>
#: en/uptodate.dbk:190
#, no-wrap
msgid "log /var/log/dpkg.log\n"
msgstr "log /var/log/dpkg.log\n"

#. type: Content of: <chapter><section><para>
#: en/uptodate.dbk:193
msgid ""
"to your <filename>/etc/dpkg/dpkg.cfg</filename>.  Be sure the created "
"logfile gets rotated periodically.  If you're using <command>logrotate</"
"command>, this can be achieved by creating a file <filename>/etc/logrotate.d/"
"dpkg</filename> with the following lines"
msgstr ""
"ao seu ficheiro <filename>/etc/dpkg/dpkg.cfg</filename>. Certifique que o "
"logfile é rodado periodicamente. Se você está a usar <command>logrotate</"
"command>, isso pode ser conseguido ao criar o ficheiro <filename>/etc/"
"logrotate.d/dpkg</filename> com as seguintes linhas"

#. type: Content of: <chapter><section><screen>
#: en/uptodate.dbk:199
#, no-wrap
msgid ""
"/var/log/dpkg {\n"
"  missingok\n"
"  notifempty\n"
"}\n"
msgstr ""
"/var/log/dpkg {\n"
"  missingok\n"
"  notifempty\n"
"}\n"

#. type: Content of: <chapter><section><para>
#: en/uptodate.dbk:205
msgid ""
"More details on <command>dpkg</command> logging can be found in the "
"<citerefentry><refentrytitle>dpkg</refentrytitle><manvolnum>1</manvolnum></"
"citerefentry> manual page."
msgstr ""
"Mais detalhes sobre os relatórios do <command>dpkg</command> podem ser "
"encontrados no manual do <citerefentry><refentrytitle>dpkg</"
"refentrytitle><manvolnum>1</manvolnum></citerefentry>"

#. type: Content of: <chapter><section><para>
#: en/uptodate.dbk:210
msgid ""
"<command>aptitude</command> logs the package installations, removals, and "
"upgrades that it intends to perform to <filename>/var/log/aptitude</"
"filename>.  Note that the <emphasis>results</emphasis> of those actions are "
"not recorded in this file!"
msgstr ""
"<command>aptitude</command> regista as instalações, remoções e actualizações "
"de pacotes que pretende executar em <filename>/var/log/aptitude</filename>. "
"Note que os <emphasis>resultados</emphasis> dessa acções não são gravados "
"neste ficheiro!"

#. type: Content of: <chapter><section><para>
#: en/uptodate.dbk:216
msgid ""
"Another way to record your actions is to run your package management session "
"within the <citerefentry><refentrytitle>script</refentrytitle><manvolnum>1</"
"manvolnum></citerefentry> program."
msgstr ""
"Outro modo de gravar as suas acções é correr a sua sessão de gestão de "
"pacotes dentro do programa <citerefentry><refentrytitle>script</"
"refentrytitle><manvolnum>1</manvolnum></citerefentry>"

#. type: Content of: <chapter><section><title>
#: en/uptodate.dbk:223
msgid "Can I automatically update the system?"
msgstr "Posso actualizar automaticamente o sistema?"

#. type: Content of: <chapter><section><para>
#: en/uptodate.dbk:225
msgid ""
"Yes.  You can use <command>cron-apt</command>; this tool updates the system "
"at regular intervals using a cron job.  By default it just updates the "
"package list and downloads new packages, but without installing them."
msgstr ""
"Sim. Você pode usar o <command>cron-apt</command>; esta ferramenta actualiza "
"o sistema em intervalos regulares usando uma rotina do cron. Por "
"predefinição apenas actualiza a lista de pacotes e e descarrega os novos "
"pacotes, mas sem os instalar."

#. type: Content of: <chapter><section><para>
#: en/uptodate.dbk:230
msgid ""
"Note: Automatic upgrade of packages is <emphasis role=\"strong\">NOT</"
"emphasis> recommended in <emphasis>testing</emphasis> or <emphasis>unstable</"
"emphasis> systems as this might bring unexpected behaviour and remove "
"packages without notice."
msgstr ""
"Nota: A actualização automática de pacotes <emphasis role=\"strong\">NÃO</"
"emphasis> é recomendada em sistemas <emphasis>testing</emphasis> ou "
"<emphasis>unstable</emphasis> pois isso pode trazer comportamentos "
"inesperados e remover pacotes sem avisar."

#. type: Content of: <chapter><section><title>
#: en/uptodate.dbk:237
msgid "I have several machines; how can I download the updates only one time?"
msgstr ""
"Eu tenho várias máquinas, como posso Eu descarregar as actualizações apenas "
"uma vez?"

#. type: Content of: <chapter><section><para>
#: en/uptodate.dbk:239
msgid ""
"If you have more than one Debian machine on your network, it is useful to "
"use <command>apt-cacher</command> to keep all of your Debian systems up-to-"
"date."
msgstr ""
"Se você tem mais do que uma máquina Debian na sua rede, é útil usar o "
"<command>apt-cacher</command> para manter todos os seus sistemas Debian "
"actualizados."

#. type: Content of: <chapter><section><para>
#: en/uptodate.dbk:243
msgid ""
"<command>apt-cacher</command> reduces the bandwidth requirements of Debian "
"mirrors by restricting the frequency of Packages, Releases and Sources file "
"updates from the back end and only doing a single fetch for any file, "
"independently of the actual request from the proxy.  <command>apt-cacher</"
"command> automatically builds a Debian HTTP mirror based on requests which "
"pass through the proxy."
msgstr ""
"O <command>apt-cacher</command> reduz os requerimentos na largura de banda "
"dos espelhos Debian ao restringir a frequência de actualizações dos "
"ficheiros Packages, Releases e Sources a partir do servidor e apenas obtendo "
"uma única vez cada ficheiro, independentemente do pedido real a partir do "
"proxy.  O <command>apt-cacher</command> automaticamente cria um espelho HTTP "
"Debian baseado nos pedidos que passam pelo proxy."

#. type: Content of: <chapter><section><para>
#: en/uptodate.dbk:251
msgid ""
"Of course, you can get the same benefit if you are already using a standard "
"caching proxy and all your systems are configured to use it."
msgstr ""
"Claro que, você pode obter o mesmo benefício se já estiver a usar um proxy "
"de caching  standard e se todos os seus sistemas estiverem configurados para "
"o usar."
