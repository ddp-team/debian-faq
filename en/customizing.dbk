<?xml version='1.0' encoding='utf-8'?>
<!-- -*- DocBook -*- -->
<!DOCTYPE book PUBLIC "-//OASIS//DTD DocBook XML V4.5//EN"
    "http://www.oasis-open.org/docbook/xml/4.5/docbookx.dtd" [
    <!ENTITY % shareddata SYSTEM "../debian-faq.ent" > %shareddata;
]>

<chapter id="customizing"><title>Customizing your &debian; system</title>
<section id="papersize"><title>How can I ensure that all programs use the same paper size?</title>
<para>
Install the <systemitem role="package">libpaper1</systemitem> package, and it
will ask you for a system-wide default paper size.  This setting will be kept
in the file <literal>/etc/papersize</literal>.
</para>
<para>
Users can override the paper size setting using the
<literal>PAPERSIZE</literal> environment variable.  For details, see the manual
page
<citerefentry><refentrytitle>papersize</refentrytitle><manvolnum>5</manvolnum></citerefentry>.
</para>
</section>

<section id="hardwareaccess"><title>How can I provide access to hardware peripherals, without compromising security?</title>
<para>
Many device files in the <literal>/dev</literal> directory belong to some
predefined groups.  For example, <literal>/dev/sr0</literal> belongs to the
<literal>cdrom</literal> group.
</para>
<para>
If you want a certain user to have access to one of these devices, just add the
user to the group the device belongs to, i.e. do:
</para>
<screen>
adduser user group
</screen>
<para>
This way you won't have to change the file permissions on the device.
</para>
<para>
If you do this from within a user's shell or a GUI environment you have to
logout and login again to become an effective member of that group.  To check
which groups you belong to run <literal>groups</literal>.
</para>
<para>
Notice that, since the introduction of <literal>udev</literal> if you change
the permissions of a hardware peripheral, they might be adjusted for some
devices when the system starts; if this happens to the hardware peripherals you
are interested in, you will have to adjust the rules at
<literal>/etc/udev</literal>.
</para>
</section>

<section id="consolefont"><title>How do I load a console font on startup the Debian way?</title>
<para>
The <systemitem role="package">kbd</systemitem> package supports this, edit the
<literal>/etc/kbd/config</literal> file.
</para>
</section>

<section id="appdefaults"><title>How can I configure an X11 program's application defaults?</title>
<para>
Debian's X programs will install their application resource data in the
<literal>/etc/X11/app-defaults/</literal> directory.  If you want to customize
X applications globally, put your customizations in those files.  They are
marked as configuration files, so their contents will be preserved during
upgrades.
</para>
</section>

<section id="booting"><title>How does a Debian system boot?</title>
<para>
Like all Unices, Debian boots up by executing the program
<literal>init</literal>.  Like most Linux distributions, a default Debian
system uses <literal>systemd</literal> as the implementation of
<literal>init</literal>.  Traditional System-V style init and other methods are
also supported.  <footnote><para> In 2014, Debian changed its default init
system from System V init to systemd.  Debian 8 "jessie" in April 2015 was the
first release to ship with systemd as default init.  Four <ulink
url="https://www.debian.org/devel/tech-ctte#status">decisions</ulink> of the
Debian Technical Committee were involved: <ulink
url="https://lists.debian.org/20140211193904.GX24404@rzlab.ucr.edu">Bug
#727708</ulink> 2014-02-11: "The committee decided that the default init system
for Linux architectures in jessie should be systemd."  <ulink
url="https://lists.debian.org/20140801023630.GF12356@teltox.donarmstrong.com">Bug
#746715</ulink> 2014-08-01: "The technical committee expects maintainers to
continue to support the multiple available init systems", and merge reasonable
contributions.  <ulink
url="https://lists.debian.org/20141116001628.GO32192@teltox.donarmstrong.com">Bug
#746578</ulink> 2014-11-15: "The committee decided that systemd-shim should be
the first listed alternative dependency of libpam-systemd instead of
systemd-sysv."  This decision made it easier to keep running a non-systemd
Debian system.  <ulink
url="https://lists.debian.org/21592.61064.527547.410074@chiark.greenend.org.uk">Bug
#762194</ulink>2017-11-04: "On automatic init system switching on upgrade"
</para> </footnote>
</para>
<para>
To control the order in which services are started, traditional System-V style
Unix systems use <emphasis>runlevels</emphasis>.  These are replaced by
<emphasis>targets</emphasis> under systemd.  To display the default target to
which systemd will bring the system, run the command
</para>
<screen>
systemctl get-default
</screen>
<para>
During boot-up, systemd starts the services or other targets listed in the
default target file <literal>/lib/systemd/system/default.target</literal>.  The
files for these services and targets are installed and the service is
<emphasis>enabled</emphasis> during Debian package installation.  If you
specifically wish not to start a service during boot-up, instead of removing
the corresponding package, you can run the command
</para>
<screen>
systemctl disable <replaceable>service</replaceable>.service
</screen>
<para>
using the name of the service file installed in
<literal>/lib/systemd/system</literal> (usually based on the name of the
package).
</para>
<para>
The <emphasis>service file</emphasis>
<literal>/lib/systemd/system/rc-local.service</literal> provides an easy way to run
customized scripts in the file <literal>/etc/rc.local</literal> after boot-up,
similar to what's offered on Debian systems running System-V style init.
Beware: this script will fail if it tries to interact with the console such as
asking for a user password or trying to clear the screen.
</para>
<para>
You can check the status of any service by the command
</para>
<screen>
service <replaceable>package</replaceable> status
</screen>
<para>
.  To start or stop a service, run
</para>
<screen>
service <replaceable>package</replaceable> start
</screen>
<para>
and
</para>
<screen>
service <replaceable>package</replaceable> stop
</screen>
<para>
.  The <literal>service</literal> command works with any init system supported
on a Debian system, not just with systemd.  If you however prefer to use the
same command on any systemd-supported Linux system, for checking the status run
</para>
<screen>
systemctl status <replaceable>package</replaceable>.service
</screen>
<para>
to get the same information.
</para>
<para>
For more information on systemd for Debian, see <ulink url="https://wiki.debian.org/systemd"/>.
</para>
</section>

<section id="sysvinit"><title>And how about Debian and traditional System V init?</title>
<para>
Debian supports booting using traditional System V init, via the sysvinit-core
package.  The configuration file for System V <literal>init</literal> (which is
<literal>/etc/inittab</literal>) specifies that the first script to be executed
should be <literal>/etc/init.d/rcS</literal>.  This script runs all of the
scripts in <literal>/etc/rcS.d/</literal> by forking subprocesses to perform
initialization such as to check and to mount file systems, to load modules, to
start the network services, to set the clock, and to perform other
initialization.
</para>
<para>
After completing the boot process, <literal>init</literal> executes all start
scripts in a directory specified by the default runlevel (this runlevel is
given by the entry for <literal>id</literal> in
<literal>/etc/inittab</literal>).  Like most System V compatible Unices, Linux
has 7 runlevels:
</para>
<itemizedlist>
<listitem>
<para>
0 (halt the system),
</para>
</listitem>
<listitem>
<para>
1 (single-user mode),
</para>
</listitem>
<listitem>
<para>
2 through 5 (various multi-user modes), and
</para>
</listitem>
<listitem>
<para>
6 (reboot the system).
</para>
</listitem>
</itemizedlist>
<para>
Debian systems come with id=2, which indicates that the default runlevel will
be '2' when the multi-user state is entered, and the scripts in
<literal>/etc/rc2.d/</literal> will be run.
</para>
<para>
Debian uses dependency-based boot ordering through <command>insserv</command>,
using the LSB headers in each script under <literal>/etc/init.d/</literal>, as
well as parallel concurrent booting through the use of
<command>startpar</command> to speed up the boot process.
</para>
<para>
The scripts in any of the directories, <literal>/etc/rcN.d/</literal> are just
symbolic links back to scripts in <literal>/etc/init.d/</literal>.  However,
the <emphasis>names</emphasis> of the files in each of the
<literal>/etc/rcN.d/</literal> directories are selected to indicate the
<emphasis>way</emphasis> the scripts in <literal>/etc/init.d/</literal> will be
run.  Specifically, before entering any runlevel, all the scripts beginning
with 'K' are run; these scripts kill services.  Then all the scripts beginning
with 'S' are run; these scripts start services.  The two-digit number following
the 'K' or 'S' indicates the order in which the script is run.  Lower numbered
scripts are executed first.
</para>
<para>
This approach works because the scripts in <literal>/etc/init.d/</literal> all
take an argument which can be either "start", "stop", "reload", "restart" or
"force-reload" and will then do the task indicated by the argument.  These
scripts can be used even after a system has been booted, to control various
processes.
</para>
<para>
For example, with the argument "reload" the command
</para>
<screen>
/etc/init.d/sendmail reload
</screen>
<para>
sends the sendmail daemon a signal to reread its configuration file.
</para>
<para>
Note that <command>invoke-rc.d</command> should not be used to call the
<literal>/etc/init.d/</literal> scripts, <command>service</command> should be
used instead.
</para>
</section>

<section id="altboot"><title>And are there yet other ways of booting a Debian system?</title>
<para>
If you do like System V init, but don't like the /etc/rc?.d/* links, you could
install the <systemitem role="package">file-rc</systemitem> package.  That will
convert the links into one single configuration file /etc/runlevel.conf
instead.
</para>
<para>
If you like neither System V nor systemd, you might like <systemitem
role="package">openrc</systemitem> or <systemitem
role="package">runit</systemitem> or <systemitem
role="package">daemontools</systemitem>.
</para>
</section>

<section id="interconffiles"><title>How does the package management system deal with packages that contain configuration files for other packages?</title>
<para>
Some users wish to create, for example, a new server by installing a group of
Debian packages and a locally generated package consisting of configuration
files.  This is not generally a good idea, because <command>dpkg</command> will
not know about those configuration files if they are in a different package,
and may write conflicting configurations when one of the initial "group" of
packages is upgraded.
</para>
<para>
Instead, create a local package that modifies the configuration files of the
"group" of Debian packages of interest.  Then <command>dpkg</command> and the
rest of the package management system will see that the files have been
modified by the local "sysadmin" and will not try to overwrite them when those
packages are upgraded.
</para>
</section>

<section id="divert"><title>How do I override a file installed by a package, so that a different version can be used instead?</title>
<para>
Suppose a sysadmin or local user wishes to use a program "login-local" rather
than the program "login" provided by the Debian <systemitem
role="package">login</systemitem> package.
</para>
<para>
Do <emphasis role="strong">not</emphasis>:
</para>
<itemizedlist>
<listitem>
<para>
Overwrite <literal>/bin/login</literal> with <literal>login-local</literal>.
</para>
</listitem>
</itemizedlist>
<para>
The package management system will not know about this change, and will simply
overwrite your custom <literal>/bin/login</literal> whenever
<literal>login</literal> (or any package that provides
<literal>/bin/login</literal>) is installed or updated.
</para>
<para>
Rather, do
</para>
<itemizedlist>
<listitem>
<para>
Execute:
</para>
<screen>
dpkg-divert --divert /bin/login.debian /bin/login
</screen>
<para>
in order to cause all future installations of the Debian <systemitem
role="package">login</systemitem> package to write the file
<literal>/bin/login</literal> to <literal>/bin/login.debian</literal> instead.
</para>
</listitem>
<listitem>
<para>
Then execute:
</para>
<screen>
cp login-local /bin/login
</screen>
<para>
to move your own locally-built program into place.
</para>
</listitem>
</itemizedlist>
<para>
Run <literal>dpkg-divert --list</literal> to see which diversions are currently
active on your system.
</para>
<para>
Details are given in the manual page
<citerefentry><refentrytitle>dpkg-divert</refentrytitle><manvolnum>8</manvolnum></citerefentry>.
</para>
</section>

<section id="localpackages"><title>How can I have my locally-built package included in the list of available packages that the package management system knows about?</title>
<para>
Execute the command:
</para>
<screen>
dpkg-scanpackages BIN_DIR OVERRIDE_FILE [PATHPREFIX] > my_Packages
</screen>
<para>
where:
</para>
<itemizedlist>
<listitem>
<para>
BIN-DIR is a directory where Debian archive files (which usually have an
extension of ".deb") are stored.
</para>
</listitem>
<listitem>
<para>
OVERRIDE_FILE is a file that is edited by the distribution maintainers and is
usually stored on a Debian archive at
<literal>indices/override.main.gz</literal> for the Debian packages in the
"main" distribution.  You can ignore this for local packages.
</para>
</listitem>
<listitem>
<para>
PATHPREFIX is an <emphasis>optional</emphasis> string that can be prepended to
the <literal>my_Packages</literal> file being produced.
</para>
</listitem>
</itemizedlist>
<para>
Once you have built the file <literal>my_Packages</literal>, tell the package
management system about it by using the command:
</para>
<screen>
dpkg --merge-avail my_Packages
</screen>
<para>
If you are using APT, you can add the local repository to your
<citerefentry><refentrytitle>sources.list</refentrytitle><manvolnum>5</manvolnum></citerefentry>
file, too.
</para>
</section>

<section id="diverse"><title>Some users like mawk, others like gawk; some like vim, others like elvis; some like trn, others like tin; how does Debian support diversity?</title>
<para>
There are several cases where two packages provide two different versions of a
program, both of which provide the same core functionality.  Users might prefer
one over another out of habit, or because the user interface of one package is
somehow more pleasing than the interface of another.  Other users on the same
system might make a different choice.
</para>
<para>
Debian uses a "virtual" package system to allow system administrators to choose
(or let users choose) their favorite tools when there are two or more that
provide the same basic functionality, yet satisfy package dependency
requirements without specifying a particular package.
</para>
<para>
For example, there might exist two different versions of newsreaders on a
system.  The news server package might 'recommend' that there exist
<emphasis>some</emphasis> news reader on the system, but the choice of
<literal>tin</literal> or <literal>trn</literal> is left up to the individual
user.  This is satisfied by having both the <systemitem
role="package">tin</systemitem> and <systemitem role="package">trn</systemitem>
packages provide the virtual package <systemitem
role="package">news-reader</systemitem>.  <emphasis>Which</emphasis> program is
invoked is determined by a link pointing from a file with the virtual package
name <literal>/etc/alternatives/news-reader</literal> to the selected file,
e.g., <literal>/usr/bin/trn</literal>.
</para>
<para>
A single link is insufficient to support full use of an alternate program;
normally, manual pages, and possibly other supporting files must be selected as
well.  The Perl script <literal>update-alternatives</literal> provides a way of
ensuring that all the files associated with a specified package are selected as
a system default.
</para>
<para>
For example, to check what executables provide "x-window-manager", run:
</para>
<screen>
update-alternatives --display x-window-manager
</screen>
<para>
If you want to change it, run:
</para>
<screen>
update-alternatives --config x-window-manager
</screen>
<para>
And follow the instructions on the screen (basically, press the number next to
the entry you'd like better).
</para>
<para>
If a package doesn't register itself as a window manager for some reason (file
a bug if it's in error), or if you use a window manager from /usr/local
directory, the selections on screen won't contain your preferred entry.  You
can update the link through command line options, like this:
</para>
<screen>
update-alternatives --install /usr/bin/x-window-manager \
  x-window-manager /usr/local/bin/wmaker-cvs 50
</screen>
<para>
The first argument to "--install" option is the symlink that points to
/etc/alternatives/NAME, where NAME is the second argument.  The third argument
is the program to which /etc/alternatives/NAME should point to, and the fourth
argument is the priority (larger value means the alternative will more probably
get picked automatically).
</para>
<para>
To remove an alternative you added, simply run:
</para>
<screen>
update-alternatives --remove x-window-manager /usr/local/bin/wmaker-cvs
</screen>
</section>

</chapter>

