<?xml version='1.0' encoding='utf-8'?>
<!-- -*- DocBook -*- -->
<!DOCTYPE book PUBLIC "-//OASIS//DTD DocBook XML V4.5//EN"
    "http://www.oasis-open.org/docbook/xml/4.5/docbookx.dtd" [
    <!ENTITY % shareddata SYSTEM "../debian-faq.ent" > %shareddata;
]>


<book lang="en">

<title>The &debian; FAQ</title>

<bookinfo>

<authorgroup>
  <author>
    <authorblurb>
      <para>Authors are listed at <link linkend="authors">Debian FAQ Authors</link>.</para>
    </authorblurb>
  </author>
</authorgroup>

<releaseinfo>Version 12.1</releaseinfo>
<pubdate>October 2024</pubdate>

<abstract>
<para>
This document answers questions frequently asked about &debian;.
</para>
</abstract>

<copyright>
  <year>1996-2024</year>
  <holder>Software in the Public Interest</holder>
</copyright>

<legalnotice>
<para>
Permission is granted to make and distribute verbatim copies of this document
provided the copyright notice and this permission notice are preserved on all
copies.
</para>
<para>
Permission is granted to copy and distribute modified versions of this document
under the conditions for verbatim copying, provided that the entire resulting
derived work is distributed under the terms of a permission notice identical to
this one.
</para>
<para>
Permission is granted to copy and distribute translations of this document into
another language, under the above conditions for modified versions, except that
this permission notice may be included in translations approved by the Free
Software Foundation instead of in the original English.
</para>
</legalnotice>

</bookinfo>

<!-- XInclude list start -->
<xi:include href="basic-defs.dbk" xmlns:xi="http://www.w3.org/2003/XInclude"/>
<xi:include href="getting-debian.dbk" xmlns:xi="http://www.w3.org/2003/XInclude"/>
<xi:include href="choosing.dbk" xmlns:xi="http://www.w3.org/2003/XInclude"/>
<xi:include href="compatibility.dbk" xmlns:xi="http://www.w3.org/2003/XInclude"/>
<xi:include href="software.dbk" xmlns:xi="http://www.w3.org/2003/XInclude"/>
<xi:include href="ftparchives.dbk" xmlns:xi="http://www.w3.org/2003/XInclude"/>
<xi:include href="pkg-basics.dbk" xmlns:xi="http://www.w3.org/2003/XInclude"/>
<xi:include href="pkgtools.dbk" xmlns:xi="http://www.w3.org/2003/XInclude"/>
<xi:include href="uptodate.dbk" xmlns:xi="http://www.w3.org/2003/XInclude"/>
<xi:include href="kernel.dbk" xmlns:xi="http://www.w3.org/2003/XInclude"/>
<xi:include href="customizing.dbk" xmlns:xi="http://www.w3.org/2003/XInclude"/>
<xi:include href="support.dbk" xmlns:xi="http://www.w3.org/2003/XInclude"/>
<xi:include href="contributing.dbk" xmlns:xi="http://www.w3.org/2003/XInclude"/>
<xi:include href="redistributing.dbk" xmlns:xi="http://www.w3.org/2003/XInclude"/>
<xi:include href="nextrelease.dbk" xmlns:xi="http://www.w3.org/2003/XInclude"/>
<xi:include href="faqinfo.dbk" xmlns:xi="http://www.w3.org/2003/XInclude"/>
<!-- XInclude list end -->

</book>
