<?xml version='1.0' encoding='utf-8'?>
<!-- -*- DocBook -*- -->
<!DOCTYPE book PUBLIC "-//OASIS//DTD DocBook XML V4.5//EN"
    "http://www.oasis-open.org/docbook/xml/4.5/docbookx.dtd" [
    <!ENTITY % shareddata SYSTEM "../debian-faq.ent" > %shareddata;
]>

<chapter id="compatibility"><title>Compatibility issues</title>
<section id="arches"><title>On what hardware architectures/systems does &debian; run?</title>
<para>
&debian; includes complete source-code for all of the included
programs, so it should work on all systems which are supported by the Linux
kernel; see the <ulink
url="http://en.tldp.org/FAQ/Linux-FAQ/intro.html#DOES-LINUX-RUN-ON-MY-COMPUTER">Linux
FAQ</ulink> for details.
</para>
<para>
The current &debian; release, &release;, contains a complete, binary
distribution for the following architectures:
</para>
<itemizedlist>
<listitem>
<para>
<emphasis>amd64</emphasis>: this covers systems based on AMD 64bit CPUs with
AMD64 extension and all Intel CPUs with EM64T extension, and a common 64bit
userspace.
</para>
</listitem>
<listitem>
<para>
<emphasis>arm64</emphasis>: supports the latest 64-bit ARM-powered devices.
</para>
</listitem>
<listitem>
<para>
<emphasis>armel</emphasis>: little-endian ARM machines.
</para>
</listitem>
<listitem>
<para>
<emphasis>armhf</emphasis>: an alternative to armel for ARMv7 machines with
hard-float.
</para>
</listitem>
<listitem>
<para>
<emphasis>i386</emphasis>: this covers systems based on Intel and compatible
processors, including Intel's 386, 486, Pentium, Pentium Pro, Pentium II (both
Klamath and Celeron), and Pentium III, and most compatible processors by AMD,
Cyrix and others.
</para>
</listitem>
<listitem>
<para>
<emphasis>ia64</emphasis>: Intel IA-64 ("Itanium") computers.
</para>
</listitem>
<listitem>
<para>
<emphasis>mips</emphasis>: SGI's big-endian MIPS systems, Indy and Indigo2;
<emphasis>mipsel</emphasis>: little-endian MIPS machines, Digital DECstations.
</para>
</listitem>
<listitem>
<para>
<emphasis>powerpc</emphasis>: this covers some IBM/Motorola PowerPC machines,
including the Apple Macintosh PowerMac models, and the CHRP and PReP open
architecture machines.
</para>
</listitem>
<listitem>
<para>
<emphasis>ppc64el</emphasis>: 64-bit little-endian PowerPC port, supports
several recent PowerPC/POWER processors.
</para>
</listitem>
<listitem>
<para>
<emphasis>s390x</emphasis>: 64-bit port for IBM System z machines, replaced
s390.
</para>
</listitem>
</itemizedlist>
<para>
The development of binary distributions of Debian for
<emphasis>hurd-i386</emphasis> (for GNU Hurd kernel on i386 32-bit PCs),
<emphasis>mipsel64</emphasis> (for 64 bit MIPS in little-endian mode),
<emphasis>powerpcspe</emphasis> (port for the "Signal Processing Engine"
hardware), <emphasis>sparc64</emphasis> (for 64 bit SPARC processors),
<emphasis>sh</emphasis> (for Hitachi SuperH processors), and
<emphasis>x32</emphasis> (for amd64/x86_64 CPUs using 32-bit pointers) is
currently underway.
</para>
<para>
Support for the <emphasis>m68k</emphasis> architecture was dropped in the Etch
(Debian 4.0) release, because it did not meet the criteria set by the Debian
Release Managers.  This architecture covers Amigas and ATARIs having a Motorola
680x0 processor for x>=2; with MMU.  However, the port is still active and
available for installation even if not a part of this official stable release
and might be reactivated for future releases.
</para>
<para>
Support for the <emphasis>hppa</emphasis> (Hewlett-Packard's PA-RISC machines)
and <emphasis>alpha</emphasis> (Compaq/Digital's Alpha systems) were dropped in
the Squeeze (Debian 6.0) release for similar reasons.  The
<emphasis>arm</emphasis> was dropped too in this release, as it was superseded
by the <emphasis>armel</emphasis> architecture.
</para>
<para>
Support for the 32-bit <emphasis>s390</emphasis> port (s390) was discontinued
and replaced with s390x in Jessie (Debian 8).  In addition, the ports to IA-64
and Sparc had to be removed from this release due to insufficient developer
support.
</para>
<para>
For more information on the available ports see the <ulink
url="https://www.debian.org/ports/">ports pages at the website</ulink>.
</para>
<para>
For further information on booting, partitioning your drive, enabling PCMCIA
(PC Card) devices and similar issues please follow the instructions given in
the Installation Manual, which is available from our WWW site at <ulink url="&url-debian-installmanual;"/>.
</para>
</section>

<section id="kernels"><title>What kernels does &debian; run?</title>
<para>
Beside Linux, Debian provides a complete, binary distribution for the following
operating system kernels:
</para>
<itemizedlist>
<listitem>
<para>
FreeBSD: provided through the <emphasis>kfreebsd-amd64</emphasis> and
<emphasis>kfreebsd-i386</emphasis> ports, for 64-bit PCs and 32-bit PCs
respectively.  These ports were first released in Debian 6.0 Squeeze as a
<emphasis>technology preview</emphasis>.  However they were not part of the
Debian 8 Jessie release.
</para>
</listitem>
</itemizedlist>
<para>
In addition to these, work is in progress on the following adaptations:
</para>
<itemizedlist>
<listitem>
<para>
<emphasis>avr32</emphasis>, port to Atmel's 32-bit RISC architecture,
</para>
</listitem>
<listitem>
<para>
<emphasis>hurd-i386</emphasis>, a port for 32-bit PC.  This port will use GNU
Hurd, the new operating system being put together by the GNU group,
</para>
</listitem>
<listitem>
<para>
<emphasis>sh</emphasis>, port to Hitachi SuperH processors.
</para>
</listitem>
</itemizedlist>
<para>
There were attempts to port the distribution to the NetBSD kernel, providing
<emphasis>netbsd-i386</emphasis> (for 32-bit PCs) and
<emphasis>netbsd-alpha</emphasis> (for Alpha machines) but these ports were
never released and are currently abandoned.
</para>
<para>
For more information on the available ports see the <ulink
url="https://www.debian.org/ports/">ports pages at the website</ulink>.
</para>
</section>

<section id="otherdistribs"><title>How compatible is Debian with other distributions of Linux?</title>
<para>
Debian developers communicate with other Linux distribution creators in an
effort to maintain binary compatibility across Linux distributions.<footnote>
<para> The <ulink url="https://wiki.linuxfoundation.org/lsb/start/">Linux Standard
Base</ulink> is a specification for allowing the same binary package to be used
on multiple distributions.  After Jessie (Debian 8) was released, Debian <ulink
url="https://sources.debian.org/src/lsb/9.20170808/debian/README.Debian/">abandoned</ulink>
the pursuit of LSB compatibility.  See this <ulink
url="https://lists.debian.org/4526217.myWFlvm1rM@gyllingar">July 3, 2015
message from Didier Raboud</ulink> and the following discussion for background
information.  </para> </footnote> Most commercial Linux products run as well
under Debian as they do on the system upon which they were built.
</para>
<para>
&debian; adheres to the <ulink url="&url-pathname-fhs;">Linux Filesystem Hierarchy Standard</ulink>.
However, there is room for
interpretation in some of the rules within this standard, so there may be
slight differences between a Debian system and other Linux systems.
</para>
</section>

<section id="otherunices"><title>How source code compatible is Debian with other Unix systems?</title>
<para>
For most applications Linux source code is compatible with other Unix systems.
It supports almost everything that is available in System V Unix systems and
the free and commercial BSD-derived systems.  However in the Unix business such
claim has nearly no value because there is no way to prove it.  In the software
development area complete compatibility is required instead of compatibility in
"about most" cases.  So years ago the need for standards arose, and nowadays
POSIX.1 (IEEE Standard 1003.1-1990) is one of the major standards for source
code compatibility in Unix-like operating systems.
</para>
<para>
Linux is intended to adhere to POSIX.1, but the POSIX standards cost real money
and the POSIX.1 (and FIPS 151-2) certification is quite expensive; this made it
more difficult for the Linux developers to work on complete POSIX conformance.
The certification costs make it unlikely that Debian will get an official
conformance certification even if it completely passed the validation suite.
(The validation suite is now freely available, so it is expected that more
people will work on POSIX.1 issues.)
</para>
<para>
Unifix GmbH (Braunschweig, Germany) developed a Linux system that has been
certified to conform to FIPS 151-2 (a superset of POSIX.1).  This technology
was available in Unifix' own distribution called Unifix Linux 2.0 and in
Lasermoon's Linux-FT.
</para>
</section>

<section id="otherpackages"><title>Can I use Debian packages (".deb" files) on my Red Hat/Slackware/... Linux system? Can I use Red Hat packages (".rpm" files) on my &debian; system?</title>
<para>
Different Linux distributions use different package formats and different
package management programs.
</para>
<variablelist>
<varlistentry>
<term><emphasis role="strong">You probably can:</emphasis></term>
<listitem>
<para>
A program to unpack a Debian package onto a Linux host that is been built from
a "foreign" distribution is available, and will generally work, in the sense
that files will be unpacked.  The converse is probably also true, that is, a
program to unpack a Red Hat or Slackware package on a host that is based on
&debian; will probably succeed in unpacking the package and placing
most files in their intended directories.  This is largely a consequence of the
existence (and broad adherence to) the Linux Filesystem Hierarchy Standard.
The <ulink url="https://packages.debian.org/alien">Alien</ulink> package is used
to convert between different package formats.
</para>
</listitem>
</varlistentry>
<varlistentry>
<term><emphasis role="strong">You probably do not want to:</emphasis></term>
<listitem>
<para>
Most package managers write administrative files when they are used to unpack
an archive.  These administrative files are generally not standardized.
Therefore, the effect of unpacking a Debian package on a "foreign" host will
have unpredictable (certainly not useful) effects on the package manager on
that system.  Likewise, utilities from other distributions might succeed in
unpacking their archives on Debian systems, but will probably cause the Debian
package management system to fail when the time comes to upgrade or remove some
packages, or even simply to report exactly what packages are present on a
system.
</para>
</listitem>
</varlistentry>
<varlistentry>
<term><emphasis role="strong">A better way:</emphasis></term>
<listitem>
<para>
The Linux File System Standard (and therefore &debian;) requires that
subdirectories under <literal>/usr/local/</literal> be entirely under the
user's discretion.  Therefore, users can unpack "foreign" packages into this
directory, and then manage their configuration, upgrade and removal
individually.
</para>
</listitem>
</varlistentry>
</variablelist>
</section>

<section id="non-debian-programs"><title>How should I install a non-Debian program?</title>
<para>
Files under the directory <literal>/usr/local/</literal> are not under the
control of the Debian package management system.  Therefore, it is good
practice to place the source code for your program in /usr/local/src/.  For
example, you might extract the files for a package named "foo.tar" into the
directory <literal>/usr/local/src/foo</literal>.  After you compile them, place
the binaries in <literal>/usr/local/bin/</literal>, the libraries in
<literal>/usr/local/lib/</literal>, and the configuration files in
<literal>/usr/local/etc/</literal>.
</para>
<para>
If your programs and/or files really must be placed in some other directory,
you could still store them in <literal>/usr/local/</literal>, and build the
appropriate symbolic links from the required location to its location in
<literal>/usr/local/</literal>, e.g., you could make the link
</para>
<screen>
ln -s /usr/local/bin/foo /usr/bin/foo
</screen>
<para>
In any case, if you obtain a package whose copyright allows redistribution, you
should consider making a Debian package of it, and uploading it for the Debian
system.  Guidelines for becoming a package developer are included in the Debian
Policy manual (see <xref linkend="debiandocs"/>).
</para>
</section>

</chapter>

